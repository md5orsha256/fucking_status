package main

import (
    "os"
    "github.com/robfig/cron"
    "fmt"
    "io/ioutil"
    "encoding/json"
    "net/http"
    "github.com/getlantern/systray"
    "time"
    "errors"
    "github.com/getsentry/sentry-go"
)
// CONFIG BLOCK
var updateTime = "1h30m"
// CONFIG BLOCK END=

const(
    random = "Random"
)

var tagLsit = map[string]string{
    
}

type Quote struct {
    ID int
    Text string
}
type State struct {
    Quote *Quote
    Cron *cron.Cron
    CurrentQuoteType string
    QuoteTypesURL map[string]string
    MenuItems map[string]*systray.MenuItem
    Mute bool
}

func main(){
    sentry.Init(sentry.ClientOptions{
        Dsn: "https://be829285dc8d4dd395ebbcd41f86f126:71ce58d866f446ecbbb2d43fbccf9c48@sentry.io/1488727",
    })
    s := &State{
        CurrentQuoteType: random,
        QuoteTypesURL: map[string]string{
            random: "http://fucking-great-advice.ru/api/random/",
        },
        MenuItems: map[string]*systray.MenuItem{},
        Mute: false,
    }
    systray.Run(s.onReady, s.onExit)
}

func (s *State)onReady(){
    s.updateQuote()
    systray.SetTooltip("F*cking status")
    s.Cron = cron.New()
    s.Cron.AddFunc(fmt.Sprintf("@every %s", updateTime), s.updateQuote)
    s.Cron.Start()
    
    s.MenuItems["Update"] = systray.AddMenuItem("Update", "")
    systray.AddSeparator()
    s.MenuItems["Mute"] = systray.AddMenuItem("Mute", "")
    s.MenuItems["Exit"] = systray.AddMenuItem("Exit", "")
    
    for {
        select{
        case <-s.MenuItems["Update"].ClickedCh:
            if s.Mute{
                s.mute()
            }
            s.updateQuote()
        case <-s.MenuItems["Mute"].ClickedCh:
            s.mute()
        case <-s.MenuItems["Exit"].ClickedCh:
            os.Exit(0)
        }
    }
}
func (s *State)onExit(){}

func (s *State)mute(){
    s.Mute = !s.Mute
    if s.Mute {
        systray.SetTitle("*Muted*")
        s.MenuItems["Mute"].SetTitle("Unmute")
    } else{
        systray.SetTitle(s.Quote.Text)
        s.MenuItems["Mute"].SetTitle("Mute")
    }
}
    
func (s *State)updateQuote() {
    if s.Mute {
        return
    }
    client := &http.Client{Timeout: 10 * time.Second}
    url := s.QuoteTypesURL[s.CurrentQuoteType]
    
    res, err := client.Get(url)
    
    if err != nil{
        sentry.CaptureException(errors.New("Failed to get response from API"))
        return
    }
    
    data, err := ioutil.ReadAll(res.Body)
    
    if err != nil {
        sentry.CaptureException(errors.New("Could not read response body"))
        return
    }
    
    quote := &Quote{}
    json.Unmarshal(data, &quote)
    s.Quote = quote
    systray.SetTitle(quote.Text)
    fmt.Printf("Getting... \n %#v\n\t%#v\n\n", time.Now().Format(time.RFC850),  quote)
}